/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Sadikshya
 * 
 * 
 * firing range manage bullet object
 */
public class FiringRange 
{
    ArrayList<Bullet> bullets;
    //need to change to collection which have faster removing rate
    
    public FiringRange()
    {
        bullets=new ArrayList<>();
    }
    
    public void add(Bullet bullet)
    {
        bullets.add(bullet);
    }
    public void draw(SpriteBatch batch)
    {
        for (Bullet bullet:bullets) 
        {
            bullet.draw(batch);
        }
        //to remove after being out of range
        Iterator<Bullet> iter = bullets.iterator();

        while (iter.hasNext())
        {
             Bullet b = iter.next();
             if (b.getPosition().y>750)
             iter.remove();
        }
    }  
}
