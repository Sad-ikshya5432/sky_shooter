/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author Sadikshya
 */
public abstract class GameObject 
{
    protected Vector2 position;
    protected Vector2 velocity;
    protected int health;
    
    public GameObject()
    {
        this.position=new Vector2(0,0);
        this.velocity=new Vector2(0,0);
    }
    public Vector2 getPosition()
    {
        return this.position;
    }
    public void update()
    {
      position.add(velocity.scl(Gdx.graphics.getDeltaTime()));
    }
    public void takeDamage(int damage)
    {
        this.health-=damage;
    }
    public abstract void handleInput();
    public abstract void draw(SpriteBatch batch);
}
