/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;

/**
 *
 * @author Sadikshya
 */
public class EnemyPlane extends AnimatedGameObject 
{
    Sound explosionSound;
    private FiringRange range;
    
    public EnemyPlane(int x,int y)
    {
        super(new TextureAtlas("enemy.atlas"));
        explosionSound = Gdx.audio.newSound(Gdx.files.internal("explosion.mp3"));
        position.x=x;
        position.y=y;
        //velocity.y=-100f;   velocity.scl update value so initialization gradually decreases
        health=100;
    }
    @Override
    public void update()
    {
        velocity.y=-100f;
        super.update();
        /*
        if(this.health==0)
        {
            explosionSound.play();
        }
        */
    }
    
    @Override
    public void draw(SpriteBatch batch)
    {
        super.draw(batch);    
    }

    @Override
    public void handleInput() {
      
    }
    @Override
    public void takeDamage(int damage)
    {
         super.takeDamage(damage);
         if(this.health==0)
         {
              explosionSound.play();
              GameWorld.score+=10;
         }
            
    }
}
