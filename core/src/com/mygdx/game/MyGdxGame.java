package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


public class MyGdxGame extends Game 
{     
        OrthographicCamera camera;
        Viewport viewport;
	SpriteBatch batch;

	@Override
	public void create () 
        { 
        	//16:9 backgrond * 90
            camera=new OrthographicCamera();
            camera.position.set(405f, 720f, 0);
            camera.update();
            viewport=new FillViewport(810,1440,camera);
            viewport.apply();
            
            batch = new SpriteBatch();	
            batch.setProjectionMatrix(camera.combined);
            
            setScreen(new TitleScreen(this));
          //  gameWorld=new GameWorld();
            
	}
      /*
	@Override
	public void render () 
        {
            Gdx.gl.glClearColor(1, 0, 0, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
          //  gameWorld.handleInput();
          //  gameWorld.update();
        
            batch.setProjectionMatrix(camera.combined);
         //   batch.begin();
         //   gameWorld.draw(batch);
         //   batch.end();
           
	}
	
	@Override
	public void dispose () 
        {
          //  gameWorld.dispose();
          //  batch.dispose();
            
	}
        @Override
        public void resize(int width, int height)
        {
         /*viewport.update(width,height);
         camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0);
   }
*/
}
