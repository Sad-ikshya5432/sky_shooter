/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 *
 * @author Sadikshya
 */
public class TitleScreen implements Screen
{
    MyGdxGame game;
    Texture texture;
    Sprite sprite;
    Stage stage;
    
    public TitleScreen(MyGdxGame game)
    {
        this.game=game;
        texture=new Texture("titlescreen.png");       
        sprite=new Sprite(texture);
        /// create stage and set it as input processor
        stage = new Stage(new ScreenViewport());
	Gdx.input.setInputProcessor(stage);
    }
    @Override
    public void show() 
    {
        Table table = new Table();
		table.setFillParent(true);
        	table.setDebug(true);
        	stage.addActor(table);
        
        	// temporary until we have asset manager in
        	Skin skin = new Skin(Gdx.files.internal("shadeui/uiskin.json"));
        
        	//create buttons
        	TextButton newGame = new TextButton("New Game", skin);
        	TextButton preferences = new TextButton("Preferences", skin);
        	TextButton exit = new TextButton("Exit", skin);
        
       		//add buttons to table
        	table.add(newGame).fillX().uniformX();
		table.row().pad(10, 0, 10, 0);
		table.add(preferences).fillX().uniformX();
		table.row();
		table.add(exit).fillX().uniformX();
		
		// create button listeners
		exit.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.exit();				
			}
		});
                
		newGame.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreen(new PlayingScreen(game));			
			}
		});
		
		preferences.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				//parent.changeScreen(Box2DTutorial.PREFERENCES);					
			}
		});
                /*
       Gdx.input.setInputProcessor(new InputAdapter(){
           @Override
           public boolean keyDown(int keycode)
               {
                   if(keycode==Input.Keys.SPACE)
                       game.setScreen(new PlayingScreen(game));
                   
                   return true;
               }
        });*/
    }

    @Override
    public void render(float f) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        game.batch.begin();
        sprite.draw(game.batch);
        game.batch.end();
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
	stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void pause() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void resume() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void hide() 
    {
       Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
        texture.dispose();
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
