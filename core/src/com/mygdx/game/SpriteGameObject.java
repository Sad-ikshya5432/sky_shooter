/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 *
 * @author Sadikshya
 */
public class SpriteGameObject extends GameObject
{
    Texture texture;
    Sprite sprite;
    
    public SpriteGameObject(Texture texture)
    {
        this.texture=texture;
        this.sprite=new Sprite(texture);
    }
    @Override
    public void update() {
       
    }

    @Override
    public void handleInput() {
        
    }

    @Override
    public void draw(SpriteBatch batch) {
        
    }
    
}
