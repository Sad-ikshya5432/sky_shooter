/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.Random;

/**
 *
 * @author Sadikshya
 */
public class Enemy extends GameObject
{
    int randomNumber;
    float countDown;
    Random random;
    public Enemy()
    {
        countDown=4f;
        random = new Random();
    }
    @Override
    public void update()
    {
        super.update();
        generateRandomPlane();
    }
    @Override
    public void handleInput() {
       
    }

    @Override
    public void draw(SpriteBatch batch) {
        
    }
    public void generateRandomPlane()
    { 
        countDown -= Gdx.graphics.getDeltaTime();
        if (countDown <= 0) 
        {
           randomNumber= random.nextInt(2);
            if(randomNumber==1)
            GameWorld.addObjects(new EnemyPlane(0,1405));
              randomNumber= random.nextInt(2);
           if(randomNumber==1)
           GameWorld.addObjects(new EnemyPlane(260,1405));
           randomNumber= random.nextInt(2);
           if(randomNumber==1)
           GameWorld.addObjects(new EnemyPlane(520,1405));
           countDown += 4f; // add one second
        }
    }
    public void reset()
    {
        countDown=4f;
    }
}
