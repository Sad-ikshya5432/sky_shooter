/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 *
 * @author Sadikshya
 */
public class AnimatedGameObject extends GameObject
{
    protected TextureAtlas textureAtlas;
    protected Animation animation;
    private float elapsedTime = 0;

    public AnimatedGameObject(TextureAtlas atlas)
    {
        this.textureAtlas=atlas;
        animation = new Animation(1/60f, textureAtlas.getRegions());
    }
    @Override
    public void update() {
       super.update();
    }

    @Override
    public void handleInput() {
       
    }

    @Override
    public void draw(SpriteBatch batch) {      
        elapsedTime += Gdx.graphics.getDeltaTime();
        batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime, true), position.x, position.y);
    }
}
