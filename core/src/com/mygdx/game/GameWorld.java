/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
/**
 *
 * @author Sadikshya
 * 
 * Gameworld creates game object when it is being created
 */
public class GameWorld 
{
    Chopper chopper;
    Enemy enemy;
    EnemyPlane plane;
    Texture texture;
    Sprite sprite,scoreboard,gameover;
    static ArrayList<GameObject> objects;
    private BitmapFont font;
    static int score;
    static int lives;
    
    public GameWorld()
    {
        score=0;
        lives=5;
        objects=new ArrayList<>();
        texture=new Texture("background.png");   
        sprite=new Sprite(texture);
        texture=new Texture("Score.png");  
        scoreboard=new Sprite(texture);
        texture=new Texture("GameOver.png");
        gameover=new Sprite(texture);
        
        chopper=new Chopper();
        enemy=new Enemy();
        
        font = new BitmapFont();
        font.setColor(Color.RED);
        font.getData().setScale(2f);
       // plane=new EnemyPlane();
      //  addObjects(plane);
    }
    public void handleInput()
    {
        if(lives>0)
        chopper.handleInput();
        else
        {
            if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
                reset();
           
        }
    }
    public void update()
    {  
        if(lives>0)
        {
        chopper.update();
        enemy.update();
  //      plane.update();       
        checkCollision();
        
         //to remove after being out of range
        Iterator<GameObject> iter = objects.iterator();
        while (iter.hasNext()) 
        {
             GameObject o = iter.next();
             o.update();
             if (o.getPosition().y>1410||o.health<0||o.getPosition().y<100)
             {
                      if (o.position.y<100)
                        {
                            lives-=1;
                        }
                iter.remove();
             }
        }
        }
    }
    public void draw(SpriteBatch batch)
    {
        sprite.draw(batch);
       
        if(lives>0)
        {
        chopper.draw(batch);
    //  plane.draw(batch);
        for (GameObject obj:objects) 
        {
           if (obj.health>0) 
            obj.draw(batch);
        }
         scoreboard.draw(batch);
        font.draw(batch, "Score ::"+score,160, 1250);
        font.draw(batch, "Lives ::"+lives,640, 1250);
        }
        else
        {
            gameover.draw(batch);
            font.draw(batch, "Game Over Press Space to restart",210, 800);
            font.draw(batch, "Your Score :  "+score,340, 750);
        }
       
    }
    public void dispose()
    {
        
    }
    public static void addObjects(GameObject object)
    {
        objects.add(object);
    }
    public void checkCollision()
    {
        Rectangle object1;
        Rectangle object2;
        GameObject temp1,temp2;
        for (int i = 0; i < objects.size(); i++) 
        {
            temp1=objects.get(i);
            object1=new Rectangle(temp1.position.x,temp1.position.y,90,10);
            for(int j=1;j<objects.size();j++)
            {
                 if (i!=j) 
                {
                    temp2=objects.get(j);
               
                    object2=new Rectangle(temp2.position.x,temp2.position.y,10,10);
                    if(object2.overlaps(object1))
                    {
                        temp1.takeDamage(10);
                        temp2.takeDamage(10);
                    }
                }
              
            }
        }
    }
    public void reset()
    {
        lives=5;
        score=0;
        objects.clear();
        enemy.reset();
    }
}
