/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 *
 * @author Sadikshya
 * 
 * bullet is game object
 */
public class Bullet extends SpriteGameObject
{
    
    public Bullet(float x,float y)
    {
        super(new Texture("bullet.png"));
        health=10;
        position.x=x;
        position.y=y;
        sprite.setPosition(position.x,position.y);
        velocity.y=600f;
    }

    @Override
    public void draw(SpriteBatch batch)
    {
        sprite.draw(batch);
        sprite.translateY(19f);
        position.x=sprite.getX();
        position.y=sprite.getY();     
    }

    @Override
    public void update() 
    {
     //  super.update();
        //bullet update needs to be implemented
    }

    @Override
    public void handleInput() 
    {
        
    }
}
