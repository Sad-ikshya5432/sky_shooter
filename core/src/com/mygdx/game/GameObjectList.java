/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.ArrayList;

/**
 *
 * @author Sadikshya
 */
public class GameObjectList extends GameObject
{
    protected ArrayList<GameObject> children;
    
    public GameObjectList()
    {
        children=new ArrayList<>();
    }
    
    public void Add(GameObject obj)
    {
        children.add(obj);
    }
    public void Remove(GameObject obj)
    {
        
        children.remove(obj);
    }
    @Override
    public void update() 
    {
        
    }

    @Override
    public void handleInput() {
        
    }

    @Override
    public void draw(SpriteBatch batch) {
        
    }
    
}
