/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author Sadikshya
 * 
 * chopper is a game object
 */
public class Chopper extends AnimatedGameObject
{
    FiringRange range;
    private float fireDelay;
    
    public Chopper()
    {
        super(new TextureAtlas("player.atlas"));
        position.x=0;
        position.y=0;
       // this.range=range;   //both our chopper and enemy need range so instance of it is passed on both
        fireDelay=0f;
    }
    public void handleInput()
    {
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                velocity.x=400f;
        else if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
                velocity.x=-400f;
        else
        {
            velocity.x=0;
        }
            
        if(Gdx.input.isKeyPressed(Input.Keys.SPACE))
        {
           fireDelay -= Gdx.graphics.getDeltaTime();
            
            if (fireDelay <= 0)
            {
                GameWorld.addObjects(new Bullet(position.x+85,position.y+200));
                fireDelay += 0.175;
            }   
        }
    }
    @Override
    public void draw(SpriteBatch batch)
    {
        super.draw(batch);
    }         

    @Override
    public void update()
    {
       super.update();
    }
}
